 ![image info](img.png)
 <br>

 <br>

 Se ha puesto en el semaforo un limite de 3 hilos paralelos para que se vea mejor el funcionamiento
 <br>

 <br>
 A la izquierda (consola del servidor) se ven los ids de los clientes que actualmente se están procesando, se realiza mediante un hilo que imprime cada medio segundo. También se ven los logs del input y output en el momento que se realizan.
 <br>

 <br>
 A la derecha (consola de los clientes) aparece la información aleatoria con la que se han creado y luego se ven las respuestas obtenidas del servidor.